# Documentation BIM Extend Résa

Bienvenue sur la documentation BIM Extend Réservations

## Choisissez votre version de Revit :

[Revit 2017 ](http://bimextenddoc.readthedocs.io/projects/2017/fr/latest/)

[Revit 2018 ](http://bimextenddoc.readthedocs.io/projects/2018/fr/latest/)

[Revit 2019 ](http://bimextenddoc.readthedocs.io/projects/2019/fr/latest/)